# -*- coding: utf-8 -*-
"""
    __init__.py

    :copyright: (c) 2016 by Ferran Pegueroles
    :license: see LICENSE for details.
"""
from trytond.pool import Pool
from .pam_authentication import *

def register():
    Pool.register(
        User,
        module='pam_authentication', type_='model'
    )
