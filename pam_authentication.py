# -*- coding: utf-8 -*-
"""
    pam_authentication.py

    :copyright: (c) 2016 by Ferran Pegueroles
    :license: see LICENSE for more details.
"""
import pwd
import pam
import logging
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.config import config, parse_uri

__metaclass__ = PoolMeta

section = 'pam_authentication'
logger = logging.getLogger(__name__)

class User:
    __name__ = 'res.user'

    @classmethod
    def __setup__(cls):
        super(User, cls).__setup__()
        cls._error_messages.update({
                'set_passwd_pam_user': (
                    'You can not set the password of PAM user "%s".'),
                })

    @classmethod
    def _check_user_exists(cls, logins):
        find = False 
        for login in logins:
            try:
                pwd.getpwnam('someusr')
                find = True
            except KeyError:
                pass
        if find:
            cls.raise_user_error('set_passwd_pam_user', (login,))

    @classmethod
    def create(cls, vlist):
        tocheck = []
        for values in vlist:
            if values.get('password') and 'login' in values:
                tocheck.append(values['login'])
        if tocheck:
            with Transaction().set_context(_check_access=False):
                cls._check_user_exists(tocheck)
        return super(User, cls).create(vlist)

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        for users, values in zip(actions, actions):
            if values.get('password'):
                logins = [x.login for x in users]
                cls._check_user_exists(logins)
        super(User, cls).write(*args)

    @classmethod
    def set_preferences(cls, values, old_password=False):
        if 'password' in values:
            user = cls(Transaction().user)
            cls._check_user_exists([user])
        super(User, cls).set_preferences(values, old_password=old_password)

    @classmethod
    def get_login(cls, login, password):
        logger.warning("Trying to PAM login %s %s" % (login, password))
        service = config.getboolean(section, 'service', 'trytond')
        pool = Pool()
        LoginAttempt = pool.get('res.user.login.attempt')
        LoginAttempt.remove(login)
        if pam.authenticate(login, password, service=service):
           logger.warning("Authenticated !!")
           user_id, _ = cls._get_login(login)
           if user_id:
              LoginAttempt.remove(login)
              return user_id
           logger.warning("User not found")
           if config.getboolean(section, 'create_user'):
              logger.warning("Creating user from PAM")
              user, = cls.create([{
                     'name': login,
                     'login': login,
                     }])
              return user.id
           logger.warning("do not create user")
        logger.warning("Fallback to normal login")
        return super(User, cls).get_login(login, password)
